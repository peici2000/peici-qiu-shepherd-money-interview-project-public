package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.*;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.utils.ErrorResponseCodes;
import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.UpdateBalancePayload;
import com.shepherdmoney.interviewproject.vo.response.CreditCardView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class CreditCardController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CreditCardRepository creditCardRepository;

    Logger logger = LoggerFactory.getLogger(CreditCardController.class);

    /**
     * Handles adding credit card to user request
     * @param payload the \c AddCreditCardToUserPayload payload object
     * @return response with:
     *         1. 200 OK status code with credit card id if request is successfully handled
     *         2. 400 BAD REQUEST if request is missing info or user is not found
     *         3. 422 Unprocessable Entity if card is already associated with this user or a different user
     *         4. 500 Internal Server Error if database commit failed
     * Note that this function assumes that a credit card number can only be associated with exactly one user
     * So if a credit card is already associated with another user, this request will fail with failure reason
     * defined in \c ErrorResponseCodes
     */
    @PostMapping("/credit-card")
    public ResponseEntity<Integer> addCreditCardToUser(@RequestBody AddCreditCardToUserPayload payload) {
        String cardNumber = payload.getCardNumber();
        String cardIssuanceBank = payload.getCardIssuanceBank();
        int userId = payload.getUserId();
        // Validate request
        // 1. if card number of issuance bank is null, return bad request with reason "missing request details"
        if (cardNumber == null || cardIssuanceBank == null) {
            return ResponseEntity.badRequest().body(ErrorResponseCodes.REQUEST_MISSING_REQUIRED_FIELDS);
        }
        // 2. if user does not exist, return bad request with reason "user not found"
        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            return ResponseEntity.badRequest().body(ErrorResponseCodes.USER_NOT_FOUND);
        }
        // 3. if user already has the credit card, return 422 Unprocessable Entity with reason "card already added"
        User userUnwrapped = user.get();
        if (userUnwrapped.hasCreditCard(cardNumber)) {
            return ResponseEntity.unprocessableEntity().body(ErrorResponseCodes.CARD_ALREADY_ASSOCIATED);
        }
        // 4. if the credit is already associated with another user, return 422 Unprocessable Entity
        // with reason "card associated with another user"
        Optional<CreditCard> existingCreditCard = creditCardRepository.findByNumber(cardNumber);
        if (existingCreditCard.isPresent() && existingCreditCard.get().getOwnerId() != userId) {
            return ResponseEntity.unprocessableEntity().body(ErrorResponseCodes.CARD_ASSOCIATED_WITH_OTHER_USER);
        }

        // Handle request
        CreditCard creditCard = new CreditCard(cardIssuanceBank, cardNumber, userId);
        // try to add the current credit card to the credit card repository
        try {
            creditCardRepository.save(creditCard);
        } catch (Exception e) {
            // If database commit failed, return 500 Internal Server Error with reason "database commit failed"
            return ResponseEntity.internalServerError().body(ErrorResponseCodes.DATABASE_COMMIT_FAILED);
        }
        userUnwrapped.addCreditCard(creditCard);
        try {
            userRepository.save(userUnwrapped);
        } catch (Exception e) {
            // If database commit failed, ROLL BACK credit card creation by deleting the credit card entity
            // and return 500 Internal Server Error with reason "database commit failed"
            creditCardRepository.delete(creditCard);
            return ResponseEntity.internalServerError().body(ErrorResponseCodes.DATABASE_COMMIT_FAILED);
        }
        return ResponseEntity.ok().body(creditCard.getId());
    }

    /**
     * Get all credit cards associated with a user given user id
     * @param userId user id to search for
     * @return response with
     *          1. 200 OK with all credit card information as a list of \c CreditCardView objects
     *          2. 400 BAD REQUEST if user with given user id is not found
     */
    @GetMapping("/credit-card:all")
    public ResponseEntity<List<CreditCardView>> getAllCardOfUser(@RequestParam int userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            return ResponseEntity.badRequest().body(new ArrayList<>());
        }

        User userUnwrapped = user.get();
        Map<String, CreditCard> creditCards = userUnwrapped.getCreditCards();
        // Map each credit card to credit card view to return
        List<CreditCardView> creditCardViews = creditCards.values()
                .stream()
                .map(creditCard -> new CreditCardView(creditCard.getIssuanceBank(), creditCard.getNumber()))
                .toList();

        return ResponseEntity.badRequest().body(creditCardViews);
    }

    /**
     * Get the user id given a credit card number
     * @param creditCardNumber credit card number to search for
     * @return response with
     *          1. 200 OK with user id
     *          2. 400 BAD REQUEST if the card is not found
     */
    @GetMapping("/credit-card:user-id")
    public ResponseEntity<Integer> getUserIdForCreditCard(@RequestParam String creditCardNumber) {
        Optional<CreditCard> card = creditCardRepository.findByNumber(creditCardNumber);
        // Check if the card exists
        if (card.isEmpty()) {
            return ResponseEntity.badRequest().body(ErrorResponseCodes.CARD_NOT_FOUND);
        }
        CreditCard cardUnwrapped = card.get();
        return ResponseEntity.ok().body(cardUnwrapped.getOwnerId());
    }

    /**
     * Update balance history of credit cards
     * @param payload an array of \c UpdateBalancePayload requests
     * @return 1. 200 OK if all updates are successfully processed
     *         2. 400 BAD REQUEST if the given card number is not associated with a card.
     */
    @PostMapping("/credit-card:update-balance")
    public ResponseEntity<String> updateBalanceForCreditCard(@RequestBody UpdateBalancePayload[] payload) {
        // Given a list of transactions, update credit cards' balance history.
        //      1. For the balance history in the credit card
        //      2. If there are gaps between two balance dates, fill the empty date with the balance of the previous date
        //      3. Given the payload `payload`, calculate the balance different between the payload and the actual balance stored in the database
        //      4. If the different is not 0, update all the following budget with the difference
        //      For example: if today is 4/12, a credit card's balanceHistory is [{date: 4/12, balance: 110}, {date: 4/10, balance: 100}],
        //      Given a balance amount of {date: 4/11, amount: 110}, the new balanceHistory is
        //      [{date: 4/12, balance: 120}, {date: 4/11, balance: 110}, {date: 4/10, balance: 100}]
        //      Return 200 OK if update is done and successful, 400 Bad Request if the given card number
        //        is not associated with a card.

        for (UpdateBalancePayload update : payload) {
            String creditCardNumber = update.getCreditCardNumber();
            LocalDate balanceDate = update.getBalanceDate();
            double newBalance = update.getBalanceAmount();
            if (creditCardNumber == null || balanceDate == null) {
                return ResponseEntity.badRequest().body("malformed payload: missing credit card number of balance date");
            }
            Optional<CreditCard> optionalCard = creditCardRepository.findByNumber(creditCardNumber);
            // Check if the card exists
            if (optionalCard.isEmpty()) {
                return ResponseEntity.badRequest().body("card number: " + creditCardNumber + " is not associated with a known card");
            }
            CreditCard card = optionalCard.get();
            // If there are gaps between two balance dates, fill the empty date with the balance of the previous date
            card.fillInGapsInBalanceHistory();
            logger.debug("[CreditCardController] [Update on {}] Card number: {} has balance history after gap filling: \n{}",
                    balanceDate, card.getNumber(), card.getBalanceHistories().toString());
            // Get existing balance if any
            BalanceHistory oldBalanceHistory = card.getBalanceOnDate(balanceDate);
            if (oldBalanceHistory == null) {
                // If the update is on a date more recent than the most recent balance or older than the oldest balance
                // Create the balance for that day and continue
                card.addBalanceHistory(new BalanceHistory(balanceDate, newBalance));
                creditCardRepository.save(card);
                continue;
            }
            double oldBalance = oldBalanceHistory.getBalance();
            // Compute diff
            double diff = newBalance - oldBalance;
            if (diff != 0.0) {
                // update all the following budget with the difference
                card.updateBalanceOnOrAfterDateByDiff(balanceDate, diff);
                creditCardRepository.save(card);
                logger.debug("[CreditCardController] [Update on {}] Card number: {} has balance history after diff update of amount {}: \n{}",
                        balanceDate, card.getNumber(), diff, card.getBalanceHistories().toString());
            }
        }

        return ResponseEntity.ok().body("update completed");
    }
    
}
