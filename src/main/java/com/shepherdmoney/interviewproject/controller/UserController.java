package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.utils.ErrorResponseCodes;
import com.shepherdmoney.interviewproject.vo.request.CreateUserPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UserController {
    @Autowired
    private UserRepository repository;

    /**
     * Creates a user with username and email
     * @param payload containing required field username and email
     * @return 1. 200 OK with user id of the user created
     *         2. 400 BAD REQUEST if the request is missing username or email
     */
    @PutMapping("/user")
    public ResponseEntity<Integer> createUser(@RequestBody CreateUserPayload payload) {
        String name = payload.getName();
        String email = payload.getEmail();

        // error handling
        if (name == null || email == null) {
            return ResponseEntity.badRequest().body(ErrorResponseCodes.REQUEST_MISSING_REQUIRED_FIELDS);
        }

        // create user
        User user = new User(name, email);

        try {
            repository.save(user);
        } catch (Exception e) {
            // If database commit failed, return 500 Internal Server Error with reason "database commit failed"
            return ResponseEntity.internalServerError().body(ErrorResponseCodes.DATABASE_COMMIT_FAILED);
        }
        return ResponseEntity.ok().body(user.getId());
    }

    @DeleteMapping("/user")
    public ResponseEntity<String> deleteUser(@RequestParam int userId) {
        Optional<User> user = repository.findById(userId);

        // user not found
        if (user.isEmpty()) {
            return ResponseEntity.badRequest().body("User with userId: " + userId + " is not found.");
        }

        repository.deleteById(userId);
        return ResponseEntity.ok().body("User with userId: " + userId + " is successfully deleted.");
    }
}
