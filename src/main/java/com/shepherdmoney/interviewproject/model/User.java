package com.shepherdmoney.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Table(name = "MyUser")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private int id;

    @NonNull
    private String name;

    @NonNull
    private String email;

    @OneToMany
    private Map<String, CreditCard> creditCards = new HashMap<>();

    /**
     * Check if this user already has a credit card given card number
     * @param cardNumber credit card number to check
     * @return true if this user already has the card, false otherwise
     */
    public boolean hasCreditCard(String cardNumber) {
        return creditCards.containsKey(cardNumber);
    }

    /**
     * Adds a credit card to this user
     * @param creditCard credit card object to add
     */
    public void addCreditCard(CreditCard creditCard) {
        creditCards.put(creditCard.getNumber(), creditCard);
    }
}
