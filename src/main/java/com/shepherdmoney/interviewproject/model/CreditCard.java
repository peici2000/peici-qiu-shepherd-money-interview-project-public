package com.shepherdmoney.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Table(indexes = @Index(columnList = "number"))
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NonNull
    private String issuanceBank;

    @NonNull
    private String number;

    @NonNull
    private int ownerId;

    // Credit card's balance history. It is a requirement that the dates in the balanceHistory
    //       list must be in chronological order, with the most recent date appearing last in the list.
    //       Additionally, the last object in the "list" must have a date value that matches today's date, 
    //       since it represents the current balance of the credit card. For example:
    //       [
    //         {date: '2023-04-10', balance: 800},
    //         {date: '2023-04-11', balance: 1000},
    //         {date: '2023-04-12', balance: 1200},
    //         {date: '2023-04-13', balance: 1100},
    //         {date: '2023-04-16', balance: 900},
    //       ]
    // ADDITIONAL NOTE: For the balance history, you can use any data structure that you think is appropriate.
    //        It can be a list, array, map, pq, anything. However, there are some suggestions:
    //        1. Retrieval of a balance of a single day should be fast
    //        2. Traversal of the entire balance history should be fast
    //        3. Insertion of a new balance should be fast
    //        4. Deletion of a balance should be fast
    //        5. It is possible that there are gaps in between dates (note the 04-13 and 04-16)
    //        6. In the condition that there are gaps, retrieval of "closest" balance date should also be fast. Aka, given 4-15, return 4-16 entry tuple
    @OneToMany(cascade = CascadeType.ALL)
    private List<BalanceHistory> balanceHistories = new ArrayList<>();

    /**
     * Add a new balance history to credit
     * This assumes the balance history for this date does not exist
     * Caller is responsible for checking if balance history already exists for a date
     * @param balanceHistory balanceHistory to add
     */
    public void addBalanceHistory(BalanceHistory balanceHistory) {
        int index = Collections.binarySearch(balanceHistories, balanceHistory);
        if (index < 0) {
            index = -(index + 1);
        }
        balanceHistories.add(index, balanceHistory);
    }

    /**
     * Fill in the missing date with the balance of the previous date
     */
    public void fillInGapsInBalanceHistory() {
        int idx = 0;
        BalanceHistory prev = null;
        while (idx < balanceHistories.size()) {
            BalanceHistory curr = balanceHistories.get(idx);
            if (prev == null) {
                prev = curr;
                ++idx;
                continue;
            }
            long dayDeltaFromPrevDay = DAYS.between(prev.getDate(), curr.getDate());
            if (dayDeltaFromPrevDay > 1) {
                for (int i = 1; i < dayDeltaFromPrevDay; i++) {
                    BalanceHistory newBalanceToFillGap = new BalanceHistory(prev.getDate().plusDays(i), prev.getBalance());
                    addBalanceHistory(newBalanceToFillGap);
                }
            }
            idx += (int) dayDeltaFromPrevDay;
            prev = curr;
        }
    }

    /**
     * Get balance history for a specific date
     * @param date LocalDate to search for
     * @return Balance History for that date. null if date not found
     */
    public BalanceHistory getBalanceOnDate(LocalDate date) {
        BalanceHistory dateWrapperForSearch = new BalanceHistory(date, 0);
        int index = Collections.binarySearch(balanceHistories, dateWrapperForSearch);
        return index >= 0 ? balanceHistories.get(index) : null;
    }

    /**
     * Update credit card's balance history on and after a given date by a given amount
     * @param diffDate the date where diff is found
     * @param balanceDiff the amount of diff. Can be positive or negative
     */
    public void updateBalanceOnOrAfterDateByDiff(LocalDate diffDate, double balanceDiff) {
        for (BalanceHistory balanceHistory : balanceHistories) {
            if (balanceHistory.getDate().isEqual(diffDate) || balanceHistory.getDate().isAfter(diffDate)) {
                balanceHistory.setBalance(balanceHistory.getBalance() + balanceDiff);
            }
        }
    }

}
