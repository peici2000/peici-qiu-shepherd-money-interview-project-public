package com.shepherdmoney.interviewproject.utils;

/**
 * Definition of error codes for routes that return integer (user id / credit card id) in the happy path
 * When an error occurred, the related error code will be returned instead of actual user id / credit card id, etc
 */
public class ErrorResponseCodes {
    public static final int REQUEST_MISSING_REQUIRED_FIELDS = -1;
    public static final int USER_NOT_FOUND = -2;
    public static final int CARD_ALREADY_ASSOCIATED = -3;
    public static final int CARD_ASSOCIATED_WITH_OTHER_USER = -4;
    public static final int CARD_NOT_FOUND = -5;
    public static final int DATABASE_COMMIT_FAILED = -6;
}
